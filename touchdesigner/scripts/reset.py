# Get all OPs
selected_list = op('selected_list')
record_logic_info = op('record_logic_info')
auto_reset_timer = op('auto_reset_timer')

# Remove all selected emoji from the list
selected_list.clear()

# Set is ready to 0
record_logic_info.par.value1 = 0

# Start auto reset timer
auto_reset_timer.par.start.pulse()
