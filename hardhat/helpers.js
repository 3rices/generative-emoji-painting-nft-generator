const fetch = require('node-fetch')
const BigNumber = require('bignumber.js')
const { ethers, utils } = require('ethers')
const { getContractAt } = require('@nomiclabs/hardhat-ethers/internal/helpers')

const gasLimit = new BigNumber(getEnvVariable('GAS_LIMIT', '800000'))
  .integerValue(BigNumber.ROUND_CEIL)
  .toNumber()
const gasConf = {
  gasLimit,
}
// Get gas config
async function getFeeDataFromProvider() {
  const feeData = await getProvider().getFeeData()
  console.log('Get Fee Data from Provider:')
  console.log(`maxFeePerGas: ${utils.formatUnits(feeData.maxFeePerGas, 'gwei')}gwei`)
  console.log(
    `maxPriorityFeePerGas: ${utils.formatUnits(feeData.maxPriorityFeePerGas, 'gwei')}gwei`,
  )
  console.log(`gasPrice: ${utils.formatUnits(feeData.gasPrice, 'gwei')}gwei`)
  return feeData
}

// Get gas config
async function getGasConfig() {
  return new Promise((resolve, reject) => {
    if (getNetworkType() === 'polygon') {
      fetch(
        isTestnet()
          ? 'https://gasstation-mumbai.matic.today/v2'
          : 'https://gasstation-mainnet.matic.network/v2',
      )
        .then(response => response.json())
        .then(response => {
          console.log('getGasConfig response', response)
          const conf = {
            gasLimit,
            maxFeePerGas: new BigNumber('1000000000')
              .multipliedBy(Math.ceil(response['fast']['maxFee'] * 3))
              .integerValue(BigNumber.ROUND_CEIL)
              .toNumber(),
            maxPriorityFeePerGas: new BigNumber('1000000000')
              .multipliedBy(Math.ceil(response['fast']['maxPriorityFee'] * 2))
              .integerValue(BigNumber.ROUND_CEIL)
              .toNumber(),
          }
          console.log('Applied Fee data: ', conf)
          resolve(conf)
        })
        .catch(err => reject(err))
    } else {
      resolve(gasConf)
    }
  })
}

// Helper method for fetching environment variables from .env
function getEnvVariable(key, defaultValue) {
  if (process.env[key]) {
    return process.env[key]
  }
  if (!defaultValue) {
    throw `${key} is not defined and no default value was provided`
  }
  return defaultValue
}

function isTestnet() {
  const network = getNetwork()
  return network !== 'ethereum' && network !== 'matic'
}

function getNetworkType() {
  const network = getNetwork()
  return network === 'ethereum' || network === 'rinkeby' ? 'ethereum' : 'polygon'
}

function getNetwork() {
  const network = getEnvVariable('NETWORK')
  if (
    network !== 'ethereum' &&
    network !== 'rinkeby' &&
    network !== 'matic' &&
    network !== 'maticmum'
  ) {
    throw 'Network can only be ethereum, rinkeby, matic or maticmum'
  }
  return network
}

// Helper method for fetching a connection provider to the Ethereum network
function getProvider() {
  const network = getNetwork()
  const alchemyKey = getEnvVariable('ALCHEMY_KEY')
  try {
    return ethers.getDefaultProvider(network, {
      alchemy: alchemyKey,
    })
  } catch (error) {
    return new ethers.providers.AlchemyProvider(network, alchemyKey)
  }
}

// Helper method for fetching a wallet account using an environment variable for the PK
function getAccount() {
  return new ethers.Wallet(getEnvVariable('ACCOUNT_PRIVATE_KEY'), getProvider())
}

// Helper method for fetching a contract instance at a given address
function getContract(hre, name = undefined, address = undefined) {
  const account = getAccount()
  return getContractAt(
    hre,
    name || getEnvVariable('NFT_CONTRACT_NAME'),
    address || getEnvVariable('NFT_CONTRACT_ADDRESS'),
    account,
  )
}

module.exports = {
  isTestnet,
  getEnvVariable,
  getNetworkType,
  getNetwork,
  getProvider,
  getAccount,
  getContract,
  gasLimit,
  getGasConfig,
  getFeeDataFromProvider,
}
