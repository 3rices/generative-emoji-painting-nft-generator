import { SiteContent } from './components/site-content'
import { LargestCanvas } from './components/largest-canvas'
import { useItems } from './hooks/apiHook'
import { useEffect, useRef, useState } from 'react'
import _ from 'underscore'

export default function Index() {
  const { isLoading, items } = useItems(60)

  const itemsRef = useRef(items)
  itemsRef.current = items

  const [selectedItem, setSelectedItem] = useState(null)

  const shuffleItem = () => {
    setSelectedItem(_.sample(_.first(itemsRef.current, Math.min(itemsRef.current.length, 20))))
  }

  useEffect(() => {
    shuffleItem()
    const intervalId = setInterval(shuffleItem, 10 * 1000)
    return () => {
      clearInterval(intervalId)
    }
  }, [itemsRef.current])

  return (
    <SiteContent title="Highlighted">
      {(isLoading && !selectedItem) || !selectedItem ? (
        <div className="loader loading"></div>
      ) : (
        <LargestCanvas item={selectedItem} />
      )}
    </SiteContent>
  )
}
