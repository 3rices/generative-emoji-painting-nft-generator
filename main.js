require('dotenv').config()
const express = require('express')
const next = require('next')

const port = parseInt(process.env.PORT, 10) || 8964
const dev = process.env.NODE_ENV !== 'production'

const server = express()
const app = next({ dev })
const handle = app.getRequestHandler()

const { listingHandler, usageHandler } = require('./api/data')

//Init the web server powered by express
app.prepare().then(() => {
  server.use('/images', express.static('api/minting/uploads'))
  server.put('/minting', require('./api/minting'))
  server.get('/nfts/latest', listingHandler)
  server.get('/nfts/usage', usageHandler)
  server.get('*', (req, res) => handle(req, res))

  //start listen specified port
  server.listen(port, err => {
    if (err) throw err
    console.log(`> Ready on http://localhost:${port}`)
  })
})
