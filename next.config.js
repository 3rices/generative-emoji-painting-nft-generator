const NFT_CONTRACT_ADDRESS = process.env.NFT_CONTRACT_ADDRESS
const NETWORK = process.env.NETWORK

module.exports = {
  env: { NETWORK, NFT_CONTRACT_ADDRESS },
  pageExtensions: ['page.js', 'page.jsx'],
}
