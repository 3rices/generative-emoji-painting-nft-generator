const { task } = require('hardhat/config')
const { getAccount, getEnvVariable, getGasConfig } = require('./helpers')

task('check-balance', 'Prints out the balance of your account').setAction(async function (
  _taskArguments,
  _hre,
) {
  const account = getAccount()
  console.log(`Account balance for ${account.address}: ${await account.getBalance()}`)
})

task('deploy', 'Deploys contract').setAction(async function (taskArguments, hre) {
  const contractFactory = await hre.ethers.getContractFactory(
    getEnvVariable('NFT_CONTRACT_NAME'),
    getAccount(),
  )

  const gasConf = await getGasConfig()
  const contract = await contractFactory.deploy(gasConf)
  console.log(`Contract deployed to address: ${contract.address}`)
  console.log('Deploy transaction: ', contract.deployTransaction)

  const contractReceipt = await contract.deployTransaction.wait(2)
  console.log('Contract receipt', contractReceipt)
})
