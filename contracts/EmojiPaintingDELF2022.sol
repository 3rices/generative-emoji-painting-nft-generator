// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import '@openzeppelin/contracts/token/ERC721/ERC721.sol';
import '@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol';
import '@openzeppelin/contracts/utils/Counters.sol';
import '@openzeppelin/contracts/access/Ownable.sol';

/**
 * https://github.com/maticnetwork/pos-portal/blob/master/contracts/common/ContextMixin.sol
 */
abstract contract ContextMixin {
    function msgSender() internal view returns (address payable sender) {
        if (msg.sender == address(this)) {
            bytes memory array = msg.data;
            uint256 index = msg.data.length;
            assembly {
                // Load the 32 bytes word from memory with the address on the lower 20 bytes, and mask those.
                sender := and(mload(add(array, index)), 0xffffffffffffffffffffffffffffffffffffffff)
            }
        } else {
            sender = payable(msg.sender);
        }
        return sender;
    }
}

contract EmojiPaintingDELF2022 is ERC721URIStorage, ContextMixin, Ownable {
    using Counters for Counters.Counter;
    Counters.Counter private currentTokenId;

    constructor() ERC721('EmojiPaintingDELF2022', 'EPD') {}

    /**
     * This is used instead of msg.sender as transactions won't be sent by the original token owner, but by OpenSea.
     */
    function _msgSender() internal view override returns (address sender) {
        return ContextMixin.msgSender();
    }

    /**
     * Override isApprovedForAll to auto-approve OS's proxy contract
     */
    function isApprovedForAll(address _owner, address _operator)
        public
        view
        override
        returns (bool isOperator)
    {
        // if OpenSea's ERC721 Proxy Address is detected, auto-return true
        // for Polygon's Mainnet, use 0x58807baD0B376efc12F5AD86aAc70E78ed67deaE
        // for Polygon's Mumbai testnet, use 0xff7Ca10aF37178BdD056628eF42fD7F799fAc77c
        if (
            _operator == address(0x58807baD0B376efc12F5AD86aAc70E78ed67deaE) ||
            _operator == address(0xff7Ca10aF37178BdD056628eF42fD7F799fAc77c)
        ) {
            return true;
        }

        // otherwise, use the default ERC721.isApprovedForAll()
        return ERC721.isApprovedForAll(_owner, _operator);
    }

    function getCurrentTokenId() public view returns (uint256) {
        return currentTokenId.current();
    }

    function mintToken(address recipient) public returns (uint256) {
        currentTokenId.increment();
        uint256 newTokenId = currentTokenId.current();
        _safeMint(recipient, newTokenId);
        return newTokenId;
    }

    function setTokenUri(uint256 tokenId, string memory metadataURI) public onlyOwner {
        _setTokenURI(tokenId, metadataURI);
    }

    function destroyToken(uint256 tokenId) public onlyOwner {
        _burn(tokenId);
    }
}
