
# press 'Setup Parameters' in the OP to call this function to re-create the parameters.
def onSetupParameters(scriptOp):
    page = scriptOp.appendCustomPage('Custom')
    page.appendPulse('Runreset', label='Run Reset')
    return

# called whenever custom pulse parameter is pushed


def onPulse(par):
    op('reset').run()
    return


def onCook(scriptOp):

    last_progress = scriptOp['progress']
    if last_progress == None:
        last_progress = 0
    else:
        last_progress = last_progress.eval()

    scriptOp.clear()

    is_ready = op('ready_always_cook')['is_ready']
    recording_progress = op('minting_status')['recording_progress']
    is_minting = op('minting_status')['is_minting']
    mint_pulse = op('minting_status')['mint_pulse']

    if mint_pulse >= 1:
        cur_progress = 3
    elif is_minting >= 1:
        cur_progress = min(last_progress, 2.8999)
        cur_progress = cur_progress + (3.0 - cur_progress)*0.005
        cur_progress = min(cur_progress, 2.8999)
    else:
        cur_progress = is_ready + recording_progress

    chan_progress = scriptOp.appendChan('progress')
    chan_progress.vals = [cur_progress]

    scriptOp.rate = me.time.rate
    return
