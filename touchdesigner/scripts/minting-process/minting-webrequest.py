
def onConnect(webClientDAT):
	return
	
def onDisconnect(webClientDAT):
	return

def onResponse(webClientDAT, statusCode, headerDict, data):

	op('record_timer').par.initialize.pulse()
	op('status').par.value2 = 0
	op('status').par.value2.pulse(1, frames=30)

	webClientDAT.par.clear.pulse()
	return