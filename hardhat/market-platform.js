const fetch = require('node-fetch')
const { isTestnet, getNetworkType, getNetwork, getEnvVariable } = require('./helpers')
const sleep = m => new Promise(r => setTimeout(r, m))

function _fetchMetadataOpenSea(contractAddress, tokenId, forceUpdate) {
  const baseURL = isTestnet() ? 'https://testnets-api.opensea.io' : 'https://api.opensea.io'
  const apiURL = `${baseURL}/api/v1/asset/${contractAddress}/${tokenId}/?force_update=${forceUpdate}`

  return new Promise((resolve, reject) => {
    fetch(apiURL, {
      method: 'GET',
    })
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(err => reject(err))
  })
}

async function fetchMetadataOpenSea(contractAddress, tokenId, forceUpdate = false) {
  try {
    if (forceUpdate) {
      await _fetchMetadataOpenSea(contractAddress, tokenId, true)
      await sleep(2000)
    }
    return await _fetchMetadataOpenSea(contractAddress, tokenId, false)
  } catch (error) {
    console.error('Failed to fetch metadata through opensea API.', error)
    return null
  }
}

function _fetchMetadataRarible(contractAddress, tokenId) {
  const baseURL = isTestnet() ? 'https:/api-staging.rarible.org' : 'https://api.rarible.org'
  const networkType = getNetworkType().toUpperCase()
  const apiURL = `${baseURL}/v0.1/items/${networkType}:${contractAddress}:${tokenId}`

  return new Promise((resolve, reject) => {
    fetch(apiURL, {
      method: 'GET',
    })
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(err => reject(err))
  })
}

async function fetchMetadataRarible(contractAddress, tokenId) {
  try {
    return await _fetchMetadataRarible(contractAddress, tokenId, false)
  } catch (error) {
    console.error('Failed to fetch metadata through rarible API.', error)
    return null
  }
}

function _fetchMetadataAlchemy(contractAddress, tokenId) {
  const ALCHEMY_KEY = getEnvVariable('ALCHEMY_KEY')
  const baseURL = {
    rinkeby: `https://eth-rinkeby.alchemyapi.io/v2/${ALCHEMY_KEY}`,
    ethereum: `https://eth-mainnet.alchemyapi.io/v2/${ALCHEMY_KEY}`,
    maticmum: `https://polygon-mumbai.g.alchemy.com/v2/${ALCHEMY_KEY}`,
    matic: `https://polygon-mainnet.g.alchemy.com/v2/${ALCHEMY_KEY}`,
  }[getNetwork()]
  const tokenType = 'erc721'
  const apiURL = `${baseURL}/getNFTMetadata?contractAddress=${contractAddress}&tokenId=${tokenId}&tokenType=${tokenType}`

  console.log('Fetch Metadata Alchemy: ', apiURL)
  return new Promise((resolve, reject) => {
    fetch(apiURL, {
      method: 'GET',
    })
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(err => reject(err))
  })
}

async function fetchMetadataAlchemy(contractAddress, tokenId) {
  try {
    return await _fetchMetadataAlchemy(contractAddress, tokenId, false)
  } catch (error) {
    console.error('Failed to fetch metadata through alchemy API.', error)
    return null
  }
}

module.exports = { sleep, fetchMetadataOpenSea, fetchMetadataRarible, fetchMetadataAlchemy }
