import { SiteContent } from './components/site-content'
import { HallwayGallery } from './components/hallway-gallery'
import { useItems } from './hooks/apiHook'
import { useEffect, useRef, useState } from 'react'
import _ from 'underscore'

const itemCount = 18

export default function Hallway() {
  const { isLoading, items } = useItems(120)

  const itemsRef = useRef(items)
  itemsRef.current = items

  const [selectedItems, setSelectedItems] = useState([])

  const shuffleItem = () => {
    setSelectedItems(existingItems => {
      if (existingItems.length < itemCount) {
        return _.sample(itemsRef.current, itemCount)
      }

      const existingItemsTokenId = existingItems.map(item => parseInt(item['tokenId'], 10))
      const newItemSample = _.sample(
        itemsRef.current.filter(
          item => _.indexOf(existingItemsTokenId, parseInt(item['tokenId'], 10)) <= -1,
        ),
        1,
      )

      if (newItemSample && newItemSample.length) {
        let newItems = [...existingItems]
        newItems[Math.floor(Math.random() * existingItems.length)] = newItemSample[0]
        return newItems
      }

      return existingItems
    })
  }

  useEffect(() => {
    shuffleItem()
    const intervalId = setInterval(shuffleItem, 5 * 1000)
    return () => {
      clearInterval(intervalId)
    }
  }, [items])

  return (
    <SiteContent title="Hallway">
      {isLoading && items.length <= 0 ? (
        <div className="loader loading"></div>
      ) : (
        <HallwayGallery items={selectedItems} />
      )}
    </SiteContent>
  )
}
