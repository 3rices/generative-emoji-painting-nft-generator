def onDone(timerOp, segment, interrupt):
    op('auto_reset').run()
    timerOp.par.start.pulse()
    return


def onInitialize(timerOp):
    return 0


def onReady(timerOp):
    return


def onStart(timerOp):
    return


def onTimerPulse(timerOp, segment):
    return


def whileTimerActive(timerOp, segment, cycle, fraction):
    return


def onSegmentEnter(timerOp, segment, interrupt):
    return


def onSegmentExit(timerOp, segment, interrupt):
    return


def onCycleStart(timerOp, segment, cycle):
    return


def onCycleEndAlert(timerOp, segment, cycle, alertSegment, alertDone, interrupt):
    return


def onCycle(timerOp, segment, cycle):
    return
