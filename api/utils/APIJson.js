const getResponseBody = (data, status = 200, message = null) => {
  return {
    status,
    message,
    data,
  }
}

const successResponse = (res, data) => {
  return res.status(200).json(getResponseBody(data)).end()
}

const errorResponse = (res, data, status = 500, message = 'Unknown error.') => {
  return res.status(status).json(getResponseBody(data, status, message)).end()
}

module.exports = { successResponse, errorResponse }
