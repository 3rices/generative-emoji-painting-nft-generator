import { useState, memo, useMemo, useCallback } from 'react'
import { QRCodeSVG } from 'qrcode.react'

const Loader = () => <div className="lader">loading...</div>

const itemStyle = {
  wrapper: {
    display: 'inline-block',
    textAlign: 'center',
    padding: '15px',
    marginBottom: '30px',
  },
  img: {
    width: '480px',
    height: 'auto',
  },
  content: {
    fontSize: '18px',
    lineHeight: 1,
    margin: 10,
  },
  emoji: {
    fontSize: '28px',
    letterSpacing: '0.2em',
  },
}

const Item = memo(({ item }) => {
  const [imageLoaded, setImageLoaded] = useState(false)

  return (
    <div style={{ ...itemStyle.wrapper, opacity: imageLoaded ? 1 : 0 }}>
      <img
        src={item.imageURL}
        style={itemStyle.img}
        onLoad={useCallback(() => setImageLoaded(true), [])}
      />
      <p style={itemStyle.content}>{item.name}</p>
      <p style={{ ...itemStyle.content, ...itemStyle.emoji }}>
        {item.emojis.slice(0, Math.min(item.emojis.length, 12)).join('')}
      </p>
      <a href={item.permalink} target="_blank">
        <QRCodeSVG value={item.permalink} />
      </a>
    </div>
  )
})

export const DebugViewer = ({ isLoading = false, items = [] }) => {
  const memoItems = useMemo(
    () => items.map(item => <Item key={item.tokenId} item={item} />),
    [items],
  )

  return (
    <div className="debugViewer">
      {isLoading && (!items || items.length <= 0) ? (
        <Loader />
      ) : (
        <div style={{ textAlign: 'center' }}>{memoItems}</div>
      )}
    </div>
  )
}
