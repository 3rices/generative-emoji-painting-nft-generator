# Please ensure the first input is player info and second input is item pos
# press 'Setup Parameters' in the OP to call this function to re-create the parameters.

import math


def onSetupParameters(scriptOp):
    page = scriptOp.appendCustomPage('HitTest')
    page.appendFloat('Mindistance', label='Min Distance')
    return


def onCook(scriptOp):
    scriptOp.clear()
    scriptOp.numSamples = 1

    # Define default values
    nearest_distance = 999
    is_hit = False
    min_distance = scriptOp.par.Mindistance.eval()

    capture_scaler = op('capture_scaler')

    if int(capture_scaler['show']) >= 1:

        if len(scriptOp.inputs) > 1:
            player_info = scriptOp.inputs[0]
            item_info = scriptOp.inputs[1]

            # Only check hittest when the sample is larger than 1 (the first sample is not from human)
            if player_info.numSamples > 1:
                for sampleIndex in range(1, player_info.numSamples):

                    user_x = player_info['pos_x'][sampleIndex]
                    user_y = player_info['pos_y'][sampleIndex]
                    is_hand_closed = player_info['is_closed'][sampleIndex]
                    item_x = item_info['x']
                    item_y = item_info['y']

                    distance = math.hypot(user_x - item_x, user_y - item_y)

                    if distance <= min_distance and is_hand_closed >= 1:
                        is_hit = True
                        break

                    if distance < nearest_distance:
                        nearest_distance = distance

        # Fire the add to selected list function
        if is_hit == True:
            op('fn_add_to_selected').run()

    # init all chan
    chan_hit = scriptOp.appendChan('is_hit')
    chan_nearest_distance = scriptOp.appendChan('nearest_distance')

    # assign values
    chan_hit[0] = is_hit
    chan_nearest_distance[0] = nearest_distance
    return


def onPulse(par):
    return
