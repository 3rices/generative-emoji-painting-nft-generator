# Get all ops
selected_list = op('selected_list')
auto_reset_timer = op('auto_reset_timer')

# Remove the last two items from the list
last_count = selected_list.numRows
if(last_count >= 1):
    for x in range(max(2, int(last_count/2))):
        if(selected_list.numRows >= 1):
            selected_list.deleteRow(selected_list.numRows - 1)
    print("Selected emojis count: " + str(last_count) +
          " > " + str(selected_list.numRows))


# Restart the timer
auto_reset_timer.par.start.pulse()
