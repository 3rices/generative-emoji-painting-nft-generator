vec4 color;
vec4 liveFeed;
vec4 avgTiles;

float minDist;
float curDist;
int maxDepth;
int curIndex;

out vec4 fragColor;

void main() {
	minDist = 9999.;
	curIndex = 9999;
	maxDepth = int(uTD2DArrayInfos[0].depth.y);
	liveFeed = texture(sTD2DInputs[0], vUV.st);

	// Find the index has the min distance
	for(int i = 0; i < maxDepth; i++) {
		avgTiles = texture(sTD2DArrayInputs[0], vec3(vUV.st, i));
		curDist = distance(liveFeed, avgTiles);
		if(curDist < minDist) {
			curIndex = i;
			minDist = curDist;
		}
	}
	curIndex = min(max(0, curIndex), maxDepth);

    // Store the index by using rgb channel
    color.rgba = vec4(float(curIndex), float(curIndex), float(curIndex), 1.);
	fragColor = TDOutputSwizzle(color);
}
