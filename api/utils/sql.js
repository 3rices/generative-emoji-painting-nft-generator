const path = require('node:path')
const sqlite3 = require('sqlite3').verbose()
const _ = require('underscore')
const envNetwork = process.env.NETWORK

const envDailyQuota = parseInt(process.env.DAILY_QUOTA || 200, 10)

const datetimeQueries = {
  today_start_datetime_utc: `unixepoch(strftime('%Y-%m-%d 00:00:00+08:00', 'now', 'localtime'))`,
  today_end_datetime_utc: `unixepoch(strftime('%Y-%m-%d 23:59:59+08:00', 'now', 'localtime'))`,
}

/**
 * Open database for running SQL
 * @param {Function<Database>} context A callback function will be fired after openning the database
 */
function openDatabase(context) {
  const db = new sqlite3.Database(path.resolve(__dirname, `../../sql/nfts_${envNetwork}.db`))
  db.serialize(() => {
    db.run(`
    CREATE TABLE IF NOT EXISTS nfts (
      image TEXT UNIQUE,
      emojis TEXT,
      created_datetime INTEGER,
      network TEXT DEFAULT NULL,
      contract_address TEXT DEFAULT NULL,
      token_id INTEGER DEFAULT 0,
      name TEXT DEFAULT NULL,
      description TEXT DEFAULT NULL,
      metadata TEXT DEFAULT NULL,
      status INTEGER DEFAULT 0,
      response TEXT DEFAULT NULL,
      lastupdate_datetime INTEGER
    )`)
    context(db)
  })
  db.close()
}

/**
 * Insert a record with image and the list of emojis
 * @param {string} image Image path
 * @param {string} emojis Emojis separating with comma
 */
async function insertRecord(image, emojis) {
  return new Promise((resolve, reject) => {
    openDatabase(db => {
      const stmt = db.prepare(`
        INSERT OR IGNORE INTO nfts
        (image, emojis, created_datetime, lastupdate_datetime)
        VALUES 
        (?, ?, unixepoch(), unixepoch())`)
      stmt.run(image, emojis)
      stmt.finalize(err => {
        if (err) {
          reject(err)
        } else {
          resolve()
        }
      })
    })
  })
}

async function updateRecord(
  image,
  network,
  contractAddress,
  tokenId,
  name,
  description,
  metadata,
  response,
) {
  return new Promise((resolve, reject) => {
    openDatabase(db => {
      const stmt = db.prepare(`
      UPDATE nfts SET 
        lastupdate_datetime = unixepoch(),
        network = ?,
        contract_address = ?,
        token_id = ?,
        name = ?,
        description = ?,
        metadata = ?,
        response = ?,
        status = 1
      WHERE image = ?`)
      stmt.run(network, contractAddress, tokenId, name, description, metadata, response, image)
      stmt.finalize(err => {
        if (err) {
          reject(err)
        } else {
          resolve()
        }
      })
    })
  })
}

async function updateStatus(image, status) {
  return new Promise((resolve, reject) => {
    openDatabase(db => {
      const stmt = db.prepare(`
      UPDATE nfts SET 
        lastupdate_datetime = unixepoch(),
        status = ?
      WHERE image = ?`)
      stmt.run(status, image)
      stmt.finalize(err => {
        if (err) {
          reject(err)
        } else {
          resolve()
        }
      })
    })
  })
}

async function getDailyUsage() {
  return new Promise((resolve, reject) => {
    openDatabase(db => {
      db.get(
        `SELECT 
        COUNT(*) as count,
        ${envDailyQuota} as maxQuota,
        ${envDailyQuota} -  COUNT(*) as curQuota
        FROM nfts
        WHERE metadata IS NOT NULL AND 
        created_datetime BETWEEN ${datetimeQueries.today_start_datetime_utc} AND ${datetimeQueries.today_end_datetime_utc}
        `,
        async (err, row) => {
          if (err) {
            reject(err)
          } else {
            resolve(row)
          }
        },
      )
    })
  })
}

/**
 * Get records
 * @param {number} limit Number of records
 * @param {number} offset offset
 * @returns
 */
async function getRecords(where, limit, offset) {
  return new Promise((resolve, reject) => {
    const permalinkBaseURLs = {
      ethereum: 'https://opensea.io/assets/',
      rinkeby: 'https://testnets.opensea.io/assets/',
      matic: 'https://opensea.io/assets/matic/',
      maticmum: 'https://testnets.opensea.io/assets/mumbai/',
    }

    openDatabase(db => {
      db.all(
        `SELECT
          token_id,
          image,
          name as nft_name,
          description as nft_description,
          contract_address,
          network,
          metadata,
          emojis,
          status,
          datetime(created_datetime, 'unixepoch', 'localtime') as created
        FROM nfts
        ${typeof where !== 'undefined' && where ? `WHERE ${where}` : ''} 
        ORDER BY created_datetime DESC
        LIMIT ${limit ?? 100} OFFSET ${offset ?? 0}`,
        async (err, rows) => {
          if (!err) {
            resolve(
              rows.map(row => {
                const tokenId = row['token_id']
                const contract = row['contract_address']
                const network = row['network']
                const emojis = row['emojis']
                const image = row['image']
                const cmd =
                  parseInt(row['status'], 10) >= 1
                    ? null
                    : `npx hardhat mint --filename ${image} --emojis ${emojis}`

                return {
                  tokenId: tokenId,
                  contract: contract,
                  name: row['nft_name'],
                  description: row['nft_description'],
                  imageURL: `/images/${path.relative(`${__dirname}/../minting/uploads`, image)}`,
                  emojis,
                  created: row['created'],
                  permalink: `${permalinkBaseURLs[network]}${contract}/${tokenId}`,
                  cmd,
                }
              }),
            )
          } else {
            console.error(err)
            reject(err)
          }
        },
      )
    })
  })
}

module.exports = {
  getRecords,
  getDailyUsage,
  updateRecord,
  updateStatus,
  insertRecord,
}
