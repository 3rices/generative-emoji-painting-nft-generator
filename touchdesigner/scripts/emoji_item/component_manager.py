# Fire reset item when the component start, create or play state change to True (play)

def onStart():
    op('fn_reset_item').run()
    return


def onCreate():
    onStart()
    return


def onPlayStateChange(state):
    if state == True:
        onStart()
    return
