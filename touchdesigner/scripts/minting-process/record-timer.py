def onInitialize(timerOp):
    return 0


def onReady(timerOp):
    return


def onStart(timerOp):
    op('webapi').par.uploadfile = str(op('moviefileout').par.file)
    return


def onTimerPulse(timerOp, segment):
    return


def whileTimerActive(timerOp, segment, cycle, fraction):
    return


def onSegmentEnter(timerOp, segment, interrupt):
    return


def onSegmentExit(timerOp, segment, interrupt):
    return


def onCycleStart(timerOp, segment, cycle):
    return


def onCycleEndAlert(timerOp, segment, cycle, alertSegment, alertDone, interrupt):
    return


def onCycle(timerOp, segment, cycle):
    return


def onDone(timerOp, segment, interrupt):
    return
