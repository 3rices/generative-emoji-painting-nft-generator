def onValueChange(channel, sampleIndex, val, prev):
    auto_reset_timer = op('auto_reset_timer')
    record_logic_info = op('record_logic_info')
    count_limit = record_logic_info['count_limit']
    is_ready = bool(record_logic_info['is_ready'])

    if is_ready == False and val >= count_limit:
        record_logic_info.par.value1 = 1
        auto_reset_timer.par.start.pulse()
    elif val < count_limit:
        record_logic_info.par.value1 = 0

    return


def onOffToOn(channel, sampleIndex, val, prev):
    return


def whileOn(channel, sampleIndex, val, prev):
    return


def onOnToOff(channel, sampleIndex, val, prev):
    return


def whileOff(channel, sampleIndex, val, prev):
    return
