import re

def onCook(scriptOp):
	scriptOp.clear()

	for row in scriptOp.inputs[0].rows():
		path = str(row[0])
		if path != None:
			matches = re.search(r'.*\/([^\/]+)\.\w{3,4}$', path, re.IGNORECASE)
			if matches and len(matches.groups()) > 0:
				filename = matches.group(1)
				scriptOp.appendRow(['emojis[]',filename])

	return

def onSetupParameters(scriptOp):
	return

# called whenever custom pulse parameter is pushed
def onPulse(par):
	return
 