const { task, types } = require('hardhat/config')
const {
  getContract,
  getEnvVariable,
  getNetwork,
  gasLimit,
  getGasConfig,
  getFeeDataFromProvider,
} = require('./helpers')
const { uploadFile } = require('./storage')
const {
  fetchMetadataOpenSea,
  fetchMetadataRarible,
  fetchMetadataAlchemy,
  sleep,
} = require('./market-platform')

const { updateRecord } = require('../api/utils/sql')
/**
 * Check the gas data
 */
task('gas-fee-data', 'Check gas data').setAction(async function (taskArguments, hre) {
  console.log('Get FeeData FromProvider', await getFeeDataFromProvider())
})
/**
 * Check the gas fee
 */
task('gas-fee-checker', 'Check gas fee').setAction(async function (taskArguments, hre) {
  console.log('Current Gas Config', await getGasConfig())
})

/**
 * Task for minting a new NFT
 */
task('mint', 'Mints from the NFT contract')
  .addParam('filename', 'Filename of the GIF image')
  .addOptionalParam('emojis', 'The emojis', '')
  .addOptionalParam('address', 'The address to receive a token', '')
  .setAction(async function (taskArguments, hre) {
    console.log('Start minting....')

    // Define the address of the wallet for receiving the new NFT
    let address = taskArguments.address
    if (address.length <= 0) {
      address = getEnvVariable('NEW_NFT_RECEIVER_WALLET_ADDRESS')
    }

    // Get contract info
    const contractInfo = {
      name: getEnvVariable('NFT_CONTRACT_NAME'),
      address: getEnvVariable('NFT_CONTRACT_ADDRESS'),
    }

    // Get contract ready
    const contract = await getContract(hre, contractInfo.name, contractInfo.address)
    const mintingGasConf = await getGasConfig()
    const mintTokenResponse = await contract.mintToken(address, mintingGasConf)
    console.log(`Transaction Hash of minting: ${mintTokenResponse.hash}`)

    //Get the token from response
    console.log('Start waiting for at least two confirmations...')
    const mintTokenConfirmReceipt = await mintTokenResponse.wait(2)

    // Get token id from the receipt
    let tokenId = undefined
    for (const event of mintTokenConfirmReceipt.events) {
      if (event.event !== 'Transfer') {
        console.warn('ignoring unknown event type ', event.event)
        continue
      }
      tokenId = event.args.tokenId.toString()
      console.log(`New token Id: ${tokenId}`)
    }
    if (tokenId === undefined || !tokenId) {
      console.error('No token id found', mintTokenConfirmReceipt)
      return
    }

    // Get emojis list
    let emojis = []
    const strEmojis = taskArguments.emojis
    if (strEmojis.length > 0) {
      emojis = strEmojis.split(',')
    }

    // Upload the file and get the metadata URL with the token ID
    console.log('Start uploading the file and metadata...')
    const { metadataURL, metadataContent } = await uploadFile(
      taskArguments.filename,
      tokenId,
      emojis,
    )
    console.log(`The file and metadata have been uploaded, URL: ${metadataURL}`)

    // Attach Metadata URL to the token
    console.log(`Start attaching metadata to token(${tokenId})`)
    const setTokenGasConf = await getGasConfig()
    const setTokenUriResponse = await contract.setTokenUri(tokenId, metadataURL, setTokenGasConf)
    const tokenUriConfirmReceipt = await setTokenUriResponse.wait(2)
    console.log(tokenUriConfirmReceipt)

    // Confirm the token URI
    const currentTokenUri = await contract.tokenURI(tokenId, {
      gasLimit,
    })
    console.log(`currentTokenUri: ${currentTokenUri}`)

    // Wait for 10s
    for (let i = 10; i > 0; i--) {
      console.log(`Start fetching the metadata in ${i}s.`)
      await sleep(1000)
    }

    //Fetch the metadata in opensea
    const openseaMetadata = await fetchMetadataOpenSea(contractInfo.address, tokenId, true)
    console.log('openseaMetadata', openseaMetadata)

    //Fetch the metadata in rarible
    const raribleMetadata = await fetchMetadataRarible(contractInfo.address, tokenId)
    console.log('raribleMetadata', raribleMetadata)

    // Fetch the metadata in Alchemy
    const alchemyMetadata = await fetchMetadataAlchemy(contractInfo.address, tokenId)
    console.log('alchemyMetadata', alchemyMetadata)

    //Storing to local database
    await updateRecord(
      taskArguments.filename,
      getNetwork(),
      contractInfo.address,
      tokenId,
      metadataContent.name,
      metadataContent.description,
      JSON.stringify(alchemyMetadata),
      JSON.stringify({
        uri: currentTokenUri,
        raw: metadataContent,
        rarible: raribleMetadata,
        opensea: openseaMetadata,
        alchemy: alchemyMetadata,
      }),
    )
  })

/**
 * Task for check token URI
 */
task('get-token-uri', 'Check token URI')
  .addParam('id', 'The target token ID', 0, types.int)
  .setAction(async function (taskArguments, hre) {
    // Get contract info
    const contractInfo = {
      name: getEnvVariable('NFT_CONTRACT_NAME'),
      address: getEnvVariable('NFT_CONTRACT_ADDRESS'),
    }

    // Get the token ID
    const tokenId = taskArguments.id
    if (tokenId <= 0) {
      console.error('Invalid token ID', tokenId)
      return
    }

    // Get contract ready
    const contract = await getContract(hre, contractInfo.name, contractInfo.address)

    // Get Token Uri
    const currentTokenUri = await contract.tokenURI(tokenId, {
      gasLimit,
    })
    console.log(`Token URI for ${tokenId} is ${currentTokenUri}`)
  })

/**
 * Get the current token id
 */
task('get-current-token-id', 'Check current token id').setAction(async function (
  taskArguments,
  hre,
) {
  // Get contract info
  const contractInfo = {
    name: getEnvVariable('NFT_CONTRACT_NAME'),
    address: getEnvVariable('NFT_CONTRACT_ADDRESS'),
  }

  // Get contract ready
  const contract = await getContract(hre, contractInfo.name, contractInfo.address)

  // Get Token ID
  const currentTokenId = await contract.getCurrentTokenId({
    gasLimit,
  })
  console.log(`currentTokenId: ${currentTokenId.toNumber()}`)
})

/**
 * Transfer a token
 */
task('transfer', 'Transfer a token (It works only when the caller is the owner)')
  .addParam('address', 'The address to receive a token', '')
  .addParam('id', 'The target token ID', 0, types.int)
  .setAction(async function (taskArguments, hre) {
    // Get the addresses
    const currentAddress = getEnvVariable('NEW_NFT_RECEIVER_WALLET_ADDRESS')
    const newAddress = taskArguments.address
    if (currentAddress.length <= 0 || newAddress.length <= 0) {
      console.error('Invalid addresses, stopped transfering.')
      return
    }

    // Get the token ID
    const tokenId = taskArguments.id
    if (tokenId <= 0) {
      console.error('Invalid token ID', tokenId)
      return
    }

    // Get contract info
    const contractInfo = {
      name: getEnvVariable('NFT_CONTRACT_NAME'),
      address: getEnvVariable('NFT_CONTRACT_ADDRESS'),
    }

    // Get contract ready
    const contract = await getContract(hre, contractInfo.name, contractInfo.address)

    console.log(contract)

    // Transfer the token
    const transferGasConf = await getGasConfig()
    const safeTransferFromResponse = await contract['safeTransferFrom(address,address,uint256)'](
      currentAddress,
      newAddress,
      tokenId,
      transferGasConf,
    )
    console.log('safeTransferFromResponse', safeTransferFromResponse)

    // Wait for confirm
    const safeTransferFromConfirmReceipt = await safeTransferFromResponse.wait(2)
    console.log('safeTransferFromConfirmReceipt', safeTransferFromConfirmReceipt)
  })

/**
 * Task for check token URI
 */
task('destroy-token', 'Destroy a token')
  .addParam('id', 'The target token ID', 0, types.int)
  .setAction(async function (taskArguments, hre) {
    // Get contract info
    const contractInfo = {
      name: getEnvVariable('NFT_CONTRACT_NAME'),
      address: getEnvVariable('NFT_CONTRACT_ADDRESS'),
    }

    // Get the token ID
    const tokenId = taskArguments.id
    if (tokenId <= 0) {
      console.error('Invalid token ID', tokenId)
      return
    }

    // Get contract ready
    const contract = await getContract(hre, contractInfo.name, contractInfo.address)

    // Get Token Uri
    const destroyGasConf = await getGasConfig()
    const destroyResponse = await contract.destroyToken(tokenId, destroyGasConf)
    console.log('Destroy Response:', destroyResponse)

    // Wait for confirm
    const destroyConfirmReceipt = await destroyResponse.wait(2)
    console.log('destroyConfirmReceipt', destroyConfirmReceipt)
  })
