import random

# Get all ops
src_info = op('src_info')
item_pos_src = op('item_pos_src')
noise = op('noise1')
src_list = op('src_list')
selected_list = op('/project1/selected_list')
capture_scaler = op('capture_scaler')

# Generate a new starting position
item_pos_src.par.value0 = random.random()
item_pos_src.par.value1 = random.random()

# Generate a new seed for the moving noise
noise.par.seed = random.random() * 10

# Generate a new emoji index
selected_count = selected_list.numRows
src_count = int(src_info['src_count'])
if src_count > selected_count:
    is_generate = True
    new_index = 0
    while is_generate:
        new_index = random.randint(0, int(src_info['src_count'])-1)

        # Regenerate if the new index has been selected
        # Only when there are 30% or above items have not been selected
        repeated = False
        if src_count - selected_count > int(src_count*0.7):
            filename = src_list[new_index, 0]
            for selected_row in selected_list.rows():
                if(filename.__eq__(selected_row[0])):
                    repeated = True

        # Generate if repeated is true
        is_generate = repeated

    # Assign the index
    src_info.par.value1 = new_index

# Reset scaler
capture_scaler.par.value0 = 1
