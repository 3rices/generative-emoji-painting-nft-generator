# Introduction

This is an NFTs and generative Art project for the Creative Technologies Project (UFCFS4-30-3) for BSc (Hons) Digital Media - Full Time AY21-22 at The School for Higher and Professional Education (SHAPE) and UWE Bristol - University of West of England, developed by Lau Ngo Ting, Teddy and supervised by Ricky Ng.

## About me and the project

|                      |                                                                                                          |
| -------------------- | -------------------------------------------------------------------------------------------------------- |
| BSc (Hons)           | Digital Media                                                                                            |
| Year                 | 2021 - 2022                                                                                              |
| Module               | Creative Technologies Project (UFCFS4-30-3)                                                              |
| University           | The School for Higher and Professional Education (SHAPE) and UWE Bristol - University of West of England |
| Name                 | Lau Ngo Ting, Teddy                                                                                      |
| VTC SHAPE Student ID | `217061564`                                                                                              |
| UWE Student ID       | `21069683`                                                                                               |
| Supervisor           | Ricky Ng                                                                                                 |

## Keywords

NFTs, ERC721, Ethereum, Generative Art, Installation, TouchDesigner

---

# Development Setup

## Accounts

- [Alchemy](https://www.alchemy.com/)
- [MetaMask](https://metamask.zendesk.com)
- [NFT.STORAGE](https://nft.storage/)

## Environment

### Environment setup tool

- [Homebrew](https://brew.sh/) (For installing other dependencies, such as nvm, pyenv, redis, etc)
- [nvm](https://github.com/nvm-sh/nvm) (For switching the node version)
- [pyenv](https://github.com/pyenv/pyenv) (For switching the python verson)

### Software

- VSCode
- [VSCode Extensions: Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)
- [TouchDesigner](https://docs.derivative.ca/Release_Notes) 2021.16410

### Mandatory tool

- node.js 16.14.0
- python 3.7.x
- [mediapipe](https://google.github.io/mediapipe/getting_started/python.html)
- [Redis](https://redis.io/docs/getting-started/installation/) 7.0.0

## Setup

### NFT related

1. Run `npm i` to install all of the packages.
2. Clone the `.env.sample` and rename it as `.env`.
3. Update the value of `ALCHEMY_KEY` in `.env` file, refer to [Alchemy Doc](https://docs.alchemy.com/alchemy/introduction/getting-started).
4. Update the value of `ACCOUNT_PRIVATE_KEY` in `.env` file, refer to [MetaMask Doc](https://metamask.zendesk.com/hc/en-us/articles/360015289632).
5. Run `npx hardhat` to list all available tasks. (Optional)
6. Run `npx hardhat compile` to compile the contracts in `./contracts` folder.
7. Update the value of `NFT_CONTRACT_NAME` in `.env` file with the target contract name.
8. Run `npx hardhat deploy` to deploy the contracts in `./contracts` folder (Need only when deploying the contract to a new address).
9. Update the value of `NFT_CONTRACT_ADDRESS` in `.env` file with the contract address response from the above step.
10. Update the value of `NFT_STORAGE_API_KEY` in `.env` file, refer to [NFT.STORAGE](https://nft.storage/).
11. Run `npx hardhat mint --address {walllet-address} --filename {gif-image-full-path}` to mint a new NFT without emojis' name. (Optional)

### TouchDesigner related

1. Run `pip install mediapipe` to install the [mediapipe](https://google.github.io/mediapipe/getting_started/python.html) for hands tracking. (See below Troubleshooting section if you are going to run it on Mac M1)
2. Run `pip show mediapipe` to find the path of the package location, thing like `/{example-path}/.pyenv/versions/3.7.10/lib/python3.7/site-packages`.
3. Open TouchDesigner and open the Preferences
4. Assign the above path to the `Python 64-bit Module Path` on General tab on Preferences.
5. Uncheck the `Search External Python Path Last` to ensure the external python module can be loaded. For example `numpy`, the mediapipe requires higher version than the TouchDesigner built-in version.
6. Save and restart the TouchDesigner.
7. Update API URL on WebClientDAT(`/project1/minting_process/webapi`) according to the minting server address.

### Troubleshooting

#### Install mediapipe on Mac M1

1. Going to `Applications > Utilities` and right clicking `Terminal`. In the right-click menu, click on get-info and then tick the Open Using Rosetta checkbox.
2. Run `arch -x86_64 /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"` to install brew
3. Run `arch -x86_64 /usr/local/homebrew/bin/brew install python@3.7` to install python 3.7
4. Run `cd {project-root}/touchdesigner` (change the project root accordingly)
5. Run `arch -x86_64 /usr/local/homebrew/opt/python@3.7/bin/python3 -m venv pyvenv` to make a virtual environments
6. Run `source ./pyvenv/bin/activate` to activate a virtual environment for installing `mediapipe`
7. Run `arch -x86_64 pip3 install mediapipe` to install the `mediapipe`
8. Use the path from the virtual enviroment `./pyvenv/lib/python3.7/site-packages`

Reminded that the above path might be different from each Mac. Please pay attention on the paths that will be displayed after installing brew.

Related useful links:

- https://docs.derivative.ca/Category:Python

- https://stackoverflow.com/questions/68659865/cannot-pip-install-mediapipe-on-macos-m1

## Hints for Debug

## Fetch metadata and validate asset

There are two APIs provided by opean sea that we might use to validate the metadata. The target network can be changed by changing the base url.

### Fetch metadata

URL: `https://testnets-api.opensea.io/api/v1/asset/{contract-address}/{token-id}/`

Add `?force_update=true` at the end of the above URL to fetch the data wihtout using cache, which is useful when the token URI has been updated.

### Validate Asset

URL: `https://rinkeby-api.opensea.io/asset/{contract-address}/{token-id}/validate/`

### rinkeby - An alternative API to Opensea

URL: `https://rinkeby.rarible.com/collection/{contract-address}/items`

---

# Production and Performance Setup

Please follow the following steps to deploy the solution for production and performance.

Here are some remarks:

- Please ensure that the NFTs minting server and webserver have a stable public internet connection.

- Please ensure the TouchDesigner computer can connect to the minting server via either internal IP or even public DNS.

- Please ensure that the NFTs minting server is running before launching the TouchDesigner.

- Please do not save the TouchDesigner when closing it.

## NFTs minting server and webserver

1. Run `git reset --hard` to reset the project (If the files are managed by Git. If they are not, please redownload the project).

2. Follow the above development setup to get the accounts and environment (node.js and Redis) and tool (nvm) ready.

3. Run `nvm use` to align the current version of node engine.

4. Follow the above NFT related Setup from step 1 to 10 but only perform the step 8 to deploy new contract if necessary. You might find the old contract address by using [Ether Scan](https://etherscan.io/).

5. Update other environment configuration if necessary, such as `NETWORK` and `NEW_NFT_RECEIVER_WALLET_ADDRESS`.

6. Run `npm run start-redis` to start the redis service if it hasn't been started.

7. Run `npm i && npm run build && PORT=3000 npm start` to re-install, build, and run the server.

8. Open `http://localhost:3000` or `http://{internal-ip-address}:3000` (The port can be specified in step 6) via a latest chrome or firefox browser for dispalying a large canvas with latest NFTs, then press the Fullscreen button to enter the fullscreen mode.

9. Open `http://localhost:3000/hallway` or `http://{internal-ip-address}:3000/hallway` (The port can be specified in step 6) via a latest chrome or firefox browser for dispalying a list of canvases with latest NFTs, then press the Fullscreen button to enter the fullscreen mode.

## TouchDesigner Computer

1. Run `git reset --hard` to reset the project (If the files are managed by Git. If they are not, please redownload the project).

2. Follow the above development setup to get the TouchDesigner and environment (python and mediapipe) ready.

3. Follow above TouchDesigner related Setup (All of the steps).

4. Press `F1` to enter the Perform mode.

## Running the server on Linux as systemd service

Following the above steps (some extra steps might be needed depending the Linux versions, native node might be needed instead of nvm) to install node and redis. Run `which node` to find the absolute path of the node, and use the path in below service script, saving it into `/lib/systemd/system` folder.

```
[Unit]
Description=A node-based NFT minting web server
After=network.target

[Service]
Environment=NODE_ENV=production
ExecStart=/usr/bin/npm run serve
WorkingDirectory={absolute path of the repository}
SyslogIdentifier=minting-nft-server
User=root
Group=root
Restart=on-failure
RestartSec=30s

[Install]
WantedBy=multi-user.target
```
