import './styles/main.scss'

export default function GalleryApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}
