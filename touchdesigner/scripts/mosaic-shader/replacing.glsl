uniform vec2 InputDims;

float curIndex;
vec2 inputSize;
vec2 pixelateRes;

vec4 outputColor;
out vec4 fragColor;

void main()
{
	inputSize = uTD2DInfos[0].res.zw;
	pixelateRes = InputDims / inputSize;
	
	// Get the current index by r channel
	curIndex = texture(sTD2DInputs[0], vUV.st).r;
	
	// Set uvs for sampling the 3d texture.
	vec3 curUVS = vUV.stp;
	curUVS.s = mod((curUVS.s*(InputDims.x/pixelateRes.x)),1);
	curUVS.t = mod((curUVS.t*(InputDims.y/pixelateRes.y)),1);
	curUVS.p = curIndex;
	
    // Set the out by using the color from the texture2d array by the current UVs  
    fragColor = TDOutputSwizzle( texture( sTD2DArrayInputs[0] , curUVS) );
}
