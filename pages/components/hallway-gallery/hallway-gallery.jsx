import { useMemo } from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import { CanvasItem } from './canvas-item'

export const HallwayGallery = ({ items = [] }) => {
  const memoItems = useMemo(
    () =>
      items
        .filter(items => !!items)
        .map((item, index) => (
          <Col key={index} className="col" xs="4" md="3" lg="2">
            <CanvasItem item={item} />
          </Col>
        )),
    [items],
  )

  return (
    <Container fluid className="hallway-gallery">
      <Row className="min-vh-100 align-content-center">{memoItems}</Row>
    </Container>
  )
}
