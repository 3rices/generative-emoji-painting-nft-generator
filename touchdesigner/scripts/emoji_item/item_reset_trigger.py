# CHOP Execute
# Trigger reset when on to off


def onValueChange(channel, sampleIndex, val, prev):
    return


def onOffToOn(channel, sampleIndex, val, prev):
    return


def whileOn(channel, sampleIndex, val, prev):
    return


def onOnToOff(channel, sampleIndex, val, prev):
    return


def whileOff(channel, sampleIndex, val, prev):
    op('fn_reset_item').run()
    return
