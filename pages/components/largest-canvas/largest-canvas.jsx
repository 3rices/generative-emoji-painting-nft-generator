import { Col, Container, Row } from 'react-bootstrap'
import { useState, useCallback, useEffect } from 'react'
import { QRCodeSVG } from 'qrcode.react'

export const LargestCanvas = ({ item }) => {
  const [imageLoaded, setImageLoaded] = useState(false)

  const handleImageLoaded = () => {
    setImageLoaded(true)
  }

  useEffect(() => {
    setImageLoaded(false)
  }, [item.imageURL])

  return (
    <Container fluid className="largest-canvas d-flex justify-content-center flex-column">
      <Row className="justify-content-center align-items-end">
        <Col xs="10" lg="6">
          <div className="artwork">
            <div className="image-wrapper">
              <img
                style={{ opacity: imageLoaded ? 1 : 0 }}
                src={item.imageURL}
                onLoad={useCallback(handleImageLoaded, [item.imageURL])}
              />
              <div className={'loader' + (imageLoaded ? '' : ' loading')}></div>
            </div>
          </div>
        </Col>
        <Col xs="auto">
          <div className="paper">
            <div className="paper-inner">
              <h1>{item.name}</h1>
              <p>{item.description}</p>
              <h2>Emojis</h2>
              <p>{item.emojis.slice(0, Math.min(item.emojis.length, 12)).join('')}</p>
              <a href={item.permalink} target="_blank">
                <QRCodeSVG value={item.permalink} />
              </a>
            </div>
          </div>
        </Col>
      </Row>
    </Container>
  )
}
