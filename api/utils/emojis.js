const emojis = {
  angry: '🤬',
  anxious: '😰',
  apple: '🍎',
  baby: '👶',
  bath: '🛁',
  blackheart: '🖤',
  bomb: '💣',
  bowing: '🙇‍♂️',
  car: '🚙',
  cat: '🐱',
  cherryblossom: '🌸', //Removed from TouchDesigner
  cry: '😭',
  dance: '💃',
  diamond: '💎',
  die: '🎲',
  dog: '🐶',
  eyes: '👀',
  football: '⚽',
  fullmoon: '🌝',
  ghost: '👻',
  heart: '❤️',
  hearteyes: '😍',
  horns: '👿',
  kiss: '💏',
  moneyfly: '💸',
  monkey: '🙈',
  newmoon: '🌚',
  no: '🙅‍♂️',
  no18: '🔞',
  notepad: '🗒️',
  panda: '🐼',
  pig: '🐷',
  pignose: '🐽',
  poo: '💩',
  rabbit: '🐰',
  rain: '🌧️',
  recycling: '♻️',
  rice: '🍚',
  rollingeyes: '🙄',
  skull: '💀',
  smileface: '😀',
  sparkling: '💖',
  steamnose: '😤',
  sun: '🌞',
  sunflower: '🌻',
  uk: '🇬🇧',
  zzz: '💤',
}

const formatName = name => {
  return name ? name.replace(/.*\/([^\/]+)\.\w{3,4}$/i, '$1') : ''
}

const getEmoji = name => emojis[formatName(name)] || undefined

const getEmojis = names => {
  return names ? names.map(getEmoji).filter(emoji => !!emoji) : []
}

module.exports = { getEmojis, getEmoji }
