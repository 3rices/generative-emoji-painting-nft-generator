# me - this DAT
# scriptOp - the OP which is cooking

import numpy as np
import cv2
import mediapipe as mp
import math

mp_hands = mp.solutions.hands
hands = mp_hands.Hands(
    model_complexity=0,
    max_num_hands=2,
    min_detection_confidence=0.88,
    min_tracking_confidence=0.6
)


def onSetupParameters(scriptOp):
    page = scriptOp.appendCustomPage('OpenCV')
    page.appendTOP('Image', label='Input TOP')
    return


def onPulse(par):
    return


def dist(point_a, point_b):
    x1, y1 = point_a
    x2, y2 = point_b
    return math.hypot(x1-x2, y1-y2)


def hand_point_dist(hand, index):
    return dist((hand.landmark[index].x, hand.landmark[index].y), (hand.landmark[0].x, hand.landmark[0].y))


def is_finger_closed(hand, tip_index, mcp_index):
    return hand_point_dist(hand, tip_index) < hand_point_dist(hand, mcp_index)


def onCook(scriptOp):
    scriptOp.clear()

    if scriptOp.par.Image.eval() != None:
        input = scriptOp.par.Image.eval().numpyArray(delayed=True)
        image = cv2.cvtColor(input, cv2.COLOR_RGBA2RGB)
        image *= 255
        image = image.astype('uint8')
        results = hands.process(image)

        ids = [0]
        is_left_hand = [0]
        is_closed = [0]
        pos_x = [0]
        pos_y = [0]
        degrees = [0]

        # Getting hands
        current_hand_index = 0
        if results != None and results.multi_hand_landmarks:
            for hand in results.multi_hand_landmarks:
                finger_closed_count = 0
                finger_closed_count += 1 if is_finger_closed(hand, 7, 5) else 0
                finger_closed_count += 1 if is_finger_closed(
                    hand, 12, 9) else 0
                finger_closed_count += 1 if is_finger_closed(
                    hand, 16, 13) else 0
                finger_closed_count += 1 if is_finger_closed(
                    hand, 20, 17) else 0

                # Cosnider as clsoed when closed finger is or greater than 3
                is_closed.append(1 if finger_closed_count >= 3 else 0)

                # Detect hand side by given label
                hand_label = 'Left'
                try:
                    hand_label = results.multi_handedness[current_hand_index].classification[0].label
                except:
                    print("Failed to get the hand label")
                is_left_hand.append(1 if hand_label == 'Left' else 0)

                # Calculate the centre point of the hand
                list_x = []
                list_y = []
                for tip_mark in [0, 3, 7, 11, 15, 19]:
                    try:
                        list_x.append(hand.landmark[tip_mark].x)
                        list_y.append(hand.landmark[tip_mark].y)
                    except:
                        print(
                            "Failed to get the position with landmark index ", tip_mark)

                center_x = (max(list_x)+min(list_x))/2.
                center_y = (max(list_y)+min(list_y))/2.

                pos_x.append(center_x)
                pos_y.append(center_y)

                # Calculate the angle
                ax, ay = hand.landmark[0].x, hand.landmark[0].y
                bx, by = hand.landmark[17].x, hand.landmark[17].y
                radians = np.arctan2(ay - by, ax - bx)
                angle = radians * 180.0 / np.pi
                if hand_label == 'Left':
                    angle = (360 - angle) - 120
                else:
                    angle = angle + 60
                degrees.append(angle)

                # Append the hand index as ID for counting the number of hands if need
                ids.append(current_hand_index)
                current_hand_index += 1

        # Define all chan
        chan_pos_x = scriptOp.appendChan('pos_x')
        chan_pos_y = scriptOp.appendChan('pos_y')
        chan_is_closed = scriptOp.appendChan('is_closed')
        chan_is_left = scriptOp.appendChan('is_left')
        chan_degrees = scriptOp.appendChan('degrees')

        # Assign values
        scriptOp.numSamples = len(ids)
        chan_is_closed.vals = is_closed
        chan_pos_x.vals = pos_x
        chan_pos_y.vals = pos_y
        chan_is_left.vals = is_left_hand
        chan_degrees.vals = degrees

    scriptOp.rate = me.time.rate
    return
