/**
 * @type import('hardhat/config').HardhatUserConfig
 */

require('dotenv').config()
require('@nomiclabs/hardhat-ethers')
require('./hardhat/contracts.js')
require('./hardhat/mint.js')

const { getEnvVariable, getNetwork } = require('./hardhat/helpers')
const ALCHEMY_KEY = getEnvVariable('ALCHEMY_KEY')
const ACCOUNT_PRIVATE_KEY = getEnvVariable('ACCOUNT_PRIVATE_KEY')

module.exports = {
  solidity: '0.8.1',
  defaultNetwork: `${getNetwork()}`,
  networks: {
    hardhat: {},
    rinkeby: {
      url: `https://eth-rinkeby.alchemyapi.io/v2/${ALCHEMY_KEY}`,
      accounts: [`0x${ACCOUNT_PRIVATE_KEY}`],
    },
    ethereum: {
      chainId: 1,
      url: `https://eth-mainnet.alchemyapi.io/v2/${ALCHEMY_KEY}`,
      accounts: [`0x${ACCOUNT_PRIVATE_KEY}`],
    },
    maticmum: {
      url: `https://polygon-mumbai.g.alchemy.com/v2/${ALCHEMY_KEY}`,
      accounts: [`0x${ACCOUNT_PRIVATE_KEY}`],
    },
    matic: {
      url: `https://polygon-mainnet.g.alchemy.com/v2/${ALCHEMY_KEY}`,
      accounts: [`0x${ACCOUNT_PRIVATE_KEY}`],
    },
  },
}
