import _ from 'underscore'

const envContractAddress = process.env.NFT_CONTRACT_ADDRESS
const envNetwork = process.env.NETWORK

const raribleBaseURLs = {
  ethereum: 'https://api.rarible.org/v0.1/items/byCollection?collection=ETHEREUM:',
  rinkeby: 'https://api-staging.rarible.org/v0.1/items/byCollection?collection=ETHEREUM:',
  matic: 'https://api.rarible.org/v0.1/items/byCollection?collection=POLYGON:',
  maticmum: 'https://api-staging.rarible.org/v0.1/items/byCollection?collection=POLYGON:',
}

const openseaBaseURLs = {
  ethereum:
    'https://api.opensea.io/api/v1/assets?order_direction=desc&offset=0&limit=20&asset_contract_address=',
  rinkeby:
    'https://testnets-api.opensea.io/api/v1/assets?order_direction=desc&offset=0&limit=20&asset_contract_address=',
  matic:
    'https://api.opensea.io/api/v1/assets?order_direction=desc&offset=0&limit=20&asset_contract_address=',
  maticmum:
    'https://testnets-api.opensea.io/api/v1/assets?order_direction=desc&offset=0&limit=20&asset_contract_address=',
}

const permalinkBaseURLs = {
  ethereum: 'https://opensea.io/assets/',
  rinkeby: 'https://testnets.opensea.io/assets/',
  matic: 'https://opensea.io/assets/matic/',
  maticmum: 'https://testnets.opensea.io/assets/mumbai/',
}

const getRaribleListURL = () => {
  return `${raribleBaseURLs[envNetwork]}${envContractAddress}`
}
const getOpenseaListURL = () => {
  return `${openseaBaseURLs[envNetwork]}${envContractAddress}`
}

const formatItem = (tokenId, contract, name, description, imageURL, emojis) => ({
  tokenId,
  contract,
  name,
  description,
  imageURL,
  emojis,
  permalink: `${permalinkBaseURLs[envNetwork]}${contract}/${tokenId}`,
})

const formatRaribleItems = data => {
  return data.items
    ? data.items
        .map(({ tokenId, contract, meta }) => {
          if (
            !tokenId ||
            !contract ||
            !meta ||
            !meta['name'] ||
            !meta['description'] ||
            !meta['attributes'] ||
            !meta['content']
          ) {
            return undefined
          }

          const contractAddress = contract.replace(/.+\:(.+)$/i, '$1')
          const emojis = meta['attributes']
            .filter(attribute => {
              return attribute['key'] && attribute['key'] === 'Emoji'
            })
            .map(attribute => attribute['value'])
          if (emojis.length <= 0) {
            return undefined
          }

          const urls = meta['content']
            .filter(content => {
              return (
                content['@type'] &&
                content['@type'] === 'IMAGE' &&
                content['mimeType'] === 'image/gif'
                //&& !content['url'].startsWith('https://ipfs.io')
              )
            })
            .sort((a, b) => {
              if (a.representation === 'ORIGINAL') {
                return 1
              }
              if (b.representation === 'ORIGINAL') {
                return -1
              }
              return a.size > b.size ? -1 : 1
            })
            .map(content => content['url'] ?? undefined)
            .filter(url => !!url)

          if (urls.length <= 0) {
            return undefined
          }

          return formatItem(
            tokenId,
            contractAddress,
            meta['name'],
            meta['description'],
            urls[0],
            emojis ?? [],
          )
        })
        .filter(item => !!item)
    : []
}

const formatOpenseaItems = data => {
  return data.assets
    ? data.assets
        .map(({ token_id, asset_contract, image_url, name, description, traits }) => {
          if (
            !token_id ||
            !asset_contract ||
            !asset_contract['address'] ||
            !image_url ||
            //image_url.startsWith('https://ipfs.io') ||
            !name ||
            !description ||
            !traits
          ) {
            return undefined
          }

          const emojis = traits
            .filter(attribute => {
              return attribute['trait_type'] && attribute['trait_type'] === 'Emoji'
            })
            .map(attribute => attribute['value'])
          if (emojis.length <= 0) {
            return undefined
          }

          return formatItem(
            token_id,
            asset_contract['address'],
            name,
            description,
            image_url,
            emojis ?? [],
          )
        })
        .filter(item => !!item)
    : []
}

const formatLocalItems = data => {
  return data.assets
    ? data.assets.map(({ tokenId, contract, name, description, imageURL, emojis }) => {
        return formatItem(tokenId, contract, name, description, imageURL, emojis.split(','))
      })
    : []
}

export const fetchRaribleItems = () =>
  new Promise((resolve, reject) => {
    const apiURL = getRaribleListURL()
    console.log('fetchRaribleItems', apiURL)
    fetch(apiURL)
      .then(res => res.json())
      .then(data => formatRaribleItems(data))
      .then(items => resolve(items))
      .catch(error => {
        console.error('Failed to fetch items from Rarible', error)
        resolve([])
      })
  })

export const fetchOpenseaItems = () =>
  new Promise((resolve, reject) => {
    const apiURL = getOpenseaListURL()
    console.log('fetchOpenseaItems', apiURL)
    fetch(apiURL)
      .then(res => res.json())
      .then(data => formatOpenseaItems(data))
      .then(items => resolve(items))
      .catch(error => {
        console.error('Failed to fetch items from Opensea', error)
        resolve([])
      })
  })

export const fetchLocalItems = () =>
  new Promise((resolve, reject) => {
    const apiURL = '/nfts/latest'
    fetch(apiURL)
      .then(res => res.json())
      .then(data => formatLocalItems(data))
      .then(items => resolve(items))
      .catch(error => {
        console.error('Failed to fetch items from Local', error)
        resolve([])
      })
  })

export const fetchItems = () => {
  return new Promise(resolve => {
    fetchLocalItems().then(localItems => {
      fetchOpenseaItems().then(openseaItems => {
        fetchRaribleItems().then(raribleItems => {
          const items = _.uniq([
            ...localItems.map(item => item.tokenId),
            ...raribleItems.map(item => item.tokenId),
            ...openseaItems.map(item => item.tokenId),
          ])
            .sort((a, b) => b - a)
            .map(tokenId => {
              let item = null
              ;[localItems, openseaItems, raribleItems].forEach(items => {
                if (!item) {
                  item = _.find(localItems, item => {
                    return item.tokenId === tokenId
                  })
                }
              })

              return item ?? null
            })
            .filter(item => !!item)
          resolve(items)
        })
      })
    })
  })
}

export const fetchUsage = () =>
  new Promise((resolve, reject) => {
    const apiURL = '/nfts/usage'
    fetch(apiURL)
      .then(res => res.json())
      .then(data => resolve(data))
      .catch(error => {
        console.error('Failed to fetch usage', error)
        resolve({
          count: 0,
          maxQuota: 200,
          curQuota: 200,
        })
      })
  })
