import { useEffect, useState, useCallback } from 'react'
import { fetchItems, fetchUsage } from './apiHelper'
import _ from 'underscore'

const maxCount = 36
export const useItems = refreshTime => {
  const [isLoading, setLoading] = useState(false)
  const [items, setItems] = useState([])

  const fetchData = () => {
    setLoading(true)
    fetchItems().then(newItems => {
      setItems(existingItems => {
        const existingTokenIds = existingItems.map(item => item.tokenId)
        return _.first(
          [
            ...newItems.filter(newItem => !existingTokenIds.includes(newItem.tokenId)),
            ...existingItems,
          ],
          maxCount,
        )
      })
      setLoading(false)
    })
  }

  useEffect(() => fetchData(), [])
  useEffect(() => {
    const intervalId = setInterval(fetchData, (refreshTime ?? 60) * 1000)
    return () => {
      clearInterval(intervalId)
    }
  }, [refreshTime])

  return { isLoading, items }
}

export const useUsageCount = refreshTime => {
  const [isLoading, setLoading] = useState(false)
  const [count, setCount] = useState(0)

  const fetchData = () => {
    setLoading(true)
    fetchUsage().then(data => {
      setCount(oldCount => data.count || oldCount)
      setLoading(false)
    })
  }

  useEffect(() => fetchData(), [])
  useEffect(() => {
    const intervalId = setInterval(fetchData, (refreshTime ?? 60) * 1000)
    return () => {
      clearInterval(intervalId)
    }
  }, [refreshTime])

  return { isLoading, count }
}
