import { SiteContent } from './components/site-content'
import { useUsageCount } from './hooks/apiHook'

export default function Index() {
  const { isLoading, count } = useUsageCount(60)

  return (
    <SiteContent title="Daily Mint">
      <div className="daily-counter">
        <div className="inner">
          <h1>
            Emoji Painting
            <br />
            NFT Generative Art
          </h1>
          <h2>Daily NFT Mint Quota</h2>
          {isLoading ? (
            <div className="loader loading"></div>
          ) : (
            <p className="count">{('000' + Math.max(0, 150 - count)).slice(-3)}</p>
          )}
        </div>
      </div>
    </SiteContent>
  )
}
