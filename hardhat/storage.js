const fs = require('node:fs')
const path = require('node:path')
const { NFTStorage, File } = require('nft.storage')
const { getEnvVariable } = require('./helpers')
const API_KEY = getEnvVariable('NFT_STORAGE_API_KEY')
const standardMetadata = require('../content/metadata.json')

async function loadGifImage(filename) {
  const imagePath = filename.startsWith('/')
    ? filename
    : `${path.resolve()}/content/images/${filename}`
  return new File([await fs.promises.readFile(imagePath)], path.basename(filename), {
    type: 'image/gif',
  })
}

const sleep = m => new Promise(r => setTimeout(r, m))

async function uploadFile(filename, tokenId = undefined, emojis = []) {
  const image = await loadGifImage(filename)

  const { name, description, attributes = [] } = standardMetadata
  const metadata = {
    name: tokenId !== undefined && tokenId ? `${name} #${tokenId}` : name,
    description,
    image,
    attributes: [
      ...attributes,
      {
        trait_type: 'Created',
        value: parseInt(new Date().getTime() / 1000),
        display_type: 'date',
      },
      ...emojis.map(emoji => ({ trait_type: 'Emoji', value: emoji })),
    ],
  }

  let IPFSResponse = null
  let IPFStry = 0

  do {
    IPFStry++
    try {
      const client = new NFTStorage({ token: API_KEY })
      IPFSResponse = await client.store(metadata)
    } catch (error) {
      console.log('Failed to upload the metadata to IPFS server', error)
      IPFSResponse = null
      await sleep(5000)
    }
  } while (IPFSResponse == null && IPFStry < 3)

  /*
  console.log('IPFS: metadata.json contents:\n', IPFSResponse.data)
  console.log('IPFS: metadata.json with IPFS gateway URLs:\n', IPFSResponse.embed())
  console.log('IPFS: URL for the metadata:\n', IPFSResponse.url)
  */

  return { metadataURL: IPFSResponse.url, metadataContent: metadata }
}

module.exports = {
  uploadFile,
}
