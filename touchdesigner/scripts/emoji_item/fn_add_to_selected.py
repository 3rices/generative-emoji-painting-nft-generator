# Get all ops
image_src = op('src')
selected_list = op('/project1/selected_list')
capture_scaler = op('capture_scaler')
auto_reset_timer = op('/project1/auto_reset_timer')

# Get filename
filename = image_src.par.file

# Add filename if it has not been selected
if filename != None:
    repeated = False
    for selected_row in selected_list.rows():
        if(filename.__eq__(selected_row[0])):
            repeated = True

    if repeated == False:
        selected_list.appendRow([filename])
        auto_reset_timer.par.start.pulse()

capture_scaler.par.value0 = 0
