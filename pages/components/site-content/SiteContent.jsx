import Head from 'next/head'
import { useCallback, useRef } from 'react'
import { Button } from 'react-bootstrap'
import { useFull } from '../../hooks/fullscreen'

export const SiteContent = ({ title = undefined, children = undefined }) => {
  const largestCanvasRef = useRef(null)
  const [full, setFull] = useFull(largestCanvasRef)

  return (
    <>
      <Head>
        <title> {title && `${title} | `}Emoji Painting Gallery</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>

      {!full && (
        <Button className="btn-fullscreen" variant="primary" onClick={() => setFull(!full)}>
          Enter Full Screen
        </Button>
      )}

      <div className="wrapper" ref={largestCanvasRef}>
        {children ?? null}
      </div>
    </>
  )
}
