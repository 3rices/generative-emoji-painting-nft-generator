const envNetwork = process.env.NETWORK
const { getRecords, getDailyUsage } = require('../utils/sql')

const usageHandler = async (req, res) => {
  res
    .status(200)
    .json(await getDailyUsage())
    .end()
}

const listingHandler = async (req, res) => {
  const type = req.query.type || 'minted'

  const wheres = {
    all: 'metadata IS NULL OR metadata IS NOT NULL',
    minted: 'metadata IS NOT NULL',
    failed: 'metadata IS NULL AND status = -1',
    waiting: 'metadata IS NULL AND status = 0',
  }
  const condition = wheres[type] || wheres['minted']

  res
    .status(200)
    .json({
      type,
      condition,
      network: `${envNetwork}`,
      assets: await getRecords(condition),
    })
    .end()
}

module.exports = {
  listingHandler,
  usageHandler,
}
