import { useState, useCallback, useEffect } from 'react'

export const CanvasItem = ({ item }) => {
  const [imageLoaded, setImageLoaded] = useState(false)
  const handleImageLoaded = () => {
    setImageLoaded(true)
  }

  useEffect(() => {
    setImageLoaded(false)
  }, [item.imageURL])

  return (
    <a className="canvas-item" href={item.permalink} target="_blank">
      <div className="image-wrapper">
        <img
          src={item.imageURL}
          onLoad={useCallback(handleImageLoaded, [item.imageURL])}
          data-token-id={item.tokenId}
        />
        <span>#{item.tokenId}</span>
        <div className={'loader' + (imageLoaded ? '' : ' loading')}></div>
      </div>
    </a>
  )
}
