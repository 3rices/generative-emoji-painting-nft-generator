const fs = require('node:fs')
const path = require('node:path')
const md5 = require('js-md5')
const Queue = require('bee-queue')
const { spawn } = require('node:child_process')
const { successResponse, errorResponse } = require('../utils/APIJson')
const { getEmojis } = require('../utils/emojis')
const { insertRecord, updateStatus, getDailyUsage } = require('../utils/sql')

// Queue
const mintingQueue = new Queue('minting-factory')
mintingQueue.process((job, done) => {
  console.log(`Processing job ${job.id} with the following data`)
  console.log(job.data)

  // Check quota
  getDailyUsage()
    .then(usage => {
      // Log the quota
      console.log(`Daily usage: ${usage.count}/${usage.maxQuota}`)
      console.log(`Remaining quota of today: ${usage.curQuota}`)

      // Throw error if there is no quota
      if (usage.curQuota < 1) {
        throw new Error(`The daily quota (${usage.maxQuota}) has been reached.`)
      }

      // Start minting
      const emojis = job.data.emojis.length ? ['--emojis', job.data.emojis.join(',')] : []
      const hardhat = spawn('npx', ['hardhat', 'mint', '--filename', job.data.filePath, ...emojis])

      //Console log the output from command lines
      hardhat.stdout.on('data', data => {
        console.log(`${data}`)
      })
      hardhat.stderr.on('data', data => {
        console.error(`------ Error: ${new Date()} ------`)
        console.error(`${data}`)
      })

      //Fire done on close
      hardhat.on('close', code => {
        const success = code === 0
        if (success) {
          console.log(`Job ${job.id} is done.`)
          done(true)
        } else {
          throw new Error('Hardhat unknown error.')
        }
      })
    })
    .catch(error => {
      console.error(`Job ${job.id} is done with error(s).`, error)
      done(false)
    })
})

const createMintingJob = (filePath, emojis) => {
  console.log(`Create a new minting job for the file (${filePath}).`)

  // Insert Record to database before creating job
  insertRecord(filePath, emojis)

  const job = mintingQueue.createJob({ filePath, emojis })
  job.save()
  job.on('succeeded', success => {
    if (success) {
      console.log(`The minting process of the given file (${filePath}) is done.`)
    } else {
      updateStatus(filePath, -1)
      console.error(`Failed to mint the file(${filePath}).`)
    }
  })
}

// HTTP request
const minting = (req, res) => {
  //Emoji
  const emojis = getEmojis(req.query.emojis || [])

  // Define upload folder
  const today = new Date()
  const dd = String(today.getDate()).padStart(2, '0')
  const mm = String(today.getMonth() + 1).padStart(2, '0')
  const yyyy = today.getFullYear()
  const uploadFolder = `${__dirname}/uploads/${yyyy}${mm}${dd}`

  //Create folder if it hasn't been created
  if (!fs.existsSync(uploadFolder)) {
    fs.mkdirSync(uploadFolder, { recursive: true })
  }

  // Define file name and upload path
  const filename = req.query.filename || 'default.gif'
  const uploadFilename = filename.replace(/.+(\.\w+)$/i, `${md5(Date.now() + filename)}$1`)
  const uploadPath = path.join(uploadFolder, uploadFilename)

  // Create Write stream
  const writeStream = fs.createWriteStream(uploadPath)
  writeStream.on('error', function (err) {
    return errorResponse(res, null, err.code || 500, err.message)
  })
  req.pipe(writeStream)
  req.on('end', () => {
    fs.stat(uploadPath, (err, uploadedFileData) => {
      if (err) {
        return errorResponse(res, null, err.code || 500, err.message)
      }

      if (!uploadedFileData || uploadedFileData.size <= 0) {
        return errorResponse(res, uploadedFileData, 409, 'Failed to upload file.')
      }

      //Create Minting Job
      createMintingJob(uploadPath, [...emojis])

      //Return response
      return successResponse(res, {
        uploadFilename,
        uploadFolder,
        uploadPath,
        uploadedFileData,
      })
    })
  })
}

module.exports = minting
